import React from "react";
import "./currency.styles.css";
import { Transition, SwitchTransition } from "react-transition-group";
import styled from "styled-components";
const FadeDiv = styled.div`
  transition: 2s;
  opacity: ${({ state }) => (state === "entered" ? 1 : 0)};
  border: 1;
  border-radius: 50px;
  text-align: center;
  background-color: ${({ state }) =>
    state === "entered" ? "transparent" : "#ac32e4"};
  display: ${({ state }) => (state === "exited" ? "none" : "block")};
`;

const FadeTransition = ({ children, ...rest }) => (
  <Transition {...rest}>
    {(state) => <FadeDiv state={state}>{children}</FadeDiv>}
  </Transition>
);

const SwitchableText = ({ textToShow, customClass }) => (
  <SwitchTransition mode="out-in">
    <FadeTransition key={textToShow} timeout={250} unmountOnExit mountOnEnter>
      <p className={customClass}>{textToShow}</p>
    </FadeTransition>
  </SwitchTransition>
);

const Currency = ({
  name,
  price,
  symbol,
  marketcap,
  volume,
  image,
  priceChange,
}) => {
  return (
    <div className="currency-container">
      <div className="currency-row">
        <div className="currency">
          <img src={image} alt="crypto" />
          <h1>{name}</h1>

          <p className="currency-symbol">{symbol}</p>
        </div>
        <div className="currency-data">
          <SwitchableText
            textToShow={"$" + price}
            customClass={"currency-price"}
          />
          <SwitchableText
            textToShow={"$" + volume.toLocaleString()}
            customClass={"currency-volume"}
          />

          {priceChange < 0 ? (
            <SwitchableText
              textToShow={priceChange.toFixed(2) + "%"}
              customClass={"currency-percent red"}
            />
          ) : (
            <SwitchableText
              textToShow={priceChange.toFixed(2) + "%"}
              customClass={"currency-percent green"}
            />
          )}

          <SwitchableText
            textToShow={marketcap.toLocaleString()}
            customClass={"currency-marketcap"}
          />
        </div>
      </div>
    </div>
  );
};

export default Currency;
