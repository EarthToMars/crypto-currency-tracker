import React, { useState, useEffect } from "react";
import "./App.css";
import { Storage } from "react-jhipster";
import Currency from "./pages/currency";
import axios from "axios";

function App() {
  const [currencies, setCurrencies] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    const CACHED_CURRENCIES = Storage.local.get("CACHED_CURRENCIES");
    if (CACHED_CURRENCIES) {
      setCurrencies(CACHED_CURRENCIES);
    }
  }, []);
  useEffect(() => {
    setInterval(async () => {
      axios
        .get(
          "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&sparkline=false"
        )
        .then((res) => {
          Storage.local.set("CACHED_CURRENCIES", res.data);
          setCurrencies(res.data);
          // console.log(res.data);
        })
        .catch((error) => console.log(error));
    }, 5000);
  }, []);

  const handleChange = (e) => {
    setSearch(e.target.value);
  };

  const filteredCurrencies = currencies.filter((currency) =>
    currency.name.toLowerCase().includes(search.toLowerCase())
  );

  return (
    <div className="currency-app">
      <div className="currency-search">
        <h1 className="currency-text">Search a currency</h1>
        <form>
          <input
            className="currency-input"
            type="text"
            onChange={handleChange}
            placeholder="Search"
          />
        </form>
      </div>
      {filteredCurrencies?.length
        ? filteredCurrencies.map((currency) => {
            return (
              <Currency
                key={currency.id}
                name={currency.name}
                price={currency.current_price}
                symbol={currency.symbol}
                marketcap={currency.total_volume}
                volume={currency.market_cap}
                image={currency.image}
                priceChange={currency.price_change_percentage_24h}
              />
            );
          })
        : "Loading ..."}
    </div>
  );
}

export default App;
